#include <fstream>
#include <iostream>
#include "../../d3workerlib/d3WorkerLib/D2Worker.h"

int main()
{
	D2Worker _d2WorkerTester;

	PolygonVector testPolygons;
	testPolygons.reserve(12);

	FromToCube cube1;
	FromToCube cube2;

	cube1.from[0] = 8.2f;
	cube1.from[1] = 0.0f;
	cube1.from[2] = 4.8f;
	cube1.to[0] = 18.6f;
	cube1.to[1] = 40.0f;
	cube1.to[2] = 5.0f;
	cube1.matireal = 0;

	cube2.from[0] = 4.8f;
	cube2.from[1] = 0.0f;
	cube2.from[2] = 4.6f;
	cube2.to[0] = 22.0f;
	cube2.to[1] = 40.0f;
	cube2.to[2] = 4.8f;
	cube2.matireal = 0;

	_d2WorkerTester.handlePolygons({ cube1, cube2 }, {});

    PolygonList testResult = _d2WorkerTester.getLinearMesh();
	
    std::ofstream fout("out.txt");
	fout.clear();

	int polygonNumber = 0;
	if (testResult.empty())
		std::cout << "Incorrect out size!!1!!1";
	for (Polygon polygon : testResult)
	{
		int pointNumber = 0;
		fout << "\n{\n";
		if (polygon.empty())
			std::cout << "Incorrect out size!!1!!1";
		for (Point point : polygon)
		{
			fout << "\n\t{\n";
			fout << "\t\t" << "{"
				<< point.at(0) << ", " << point.at(1) << ", " << point.at(2)
				<< "}\n";
			pointNumber++;
			fout << "\n\t}\n";
		}
		fout << "\n}\n";
		polygonNumber++;
	}

	return 0;
}
